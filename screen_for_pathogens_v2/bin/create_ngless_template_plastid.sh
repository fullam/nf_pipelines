#!/usr/bin/env bash

cat  <<EOF > filter_bam.ngl
ngless "1.1"
import "parallel" version "1.0"
import "mocat" version "0.0"
import "samtools" version "0.0"

input = load_mocat_sample(ARGV[1])

input = preprocess(input, keep_singles=True) using |read|:
    read = smoothtrim(read, min_quality=20, window=4)
    if len(read) < 70:
        discard

mapped = map(input, fafile='/scratch/fullam/references/oleks/references/v4/concatNonRed98o70_PL_Ref_250121_v1_3.contigs.fasta')
write(samtools_sort(mapped), ofile='unfiltered.bam' )

mapped = select(mapped) using |mr|:
    mr = mr.filter(min_match_size=70, min_identity_pc=98, action={unmatch})
write(samtools_sort(mapped), ofile='filtered.bam' )

mapped_unique = select(mapped, keep_if=[{mapped}, {unique}])
write(samtools_sort(mapped_unique), ofile='unique.bam')
EOF
