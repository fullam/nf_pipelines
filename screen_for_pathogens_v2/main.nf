#!/usr/bin/env nextflow
params.workflow_version = "screen_for_pathogens_v0.0.1"
params.mitoreference = "/scratch/fullam/references/oleks/references/v4/concatNonRed96o70_MT_Ref_250121_v1_3.contigs.fasta"
params.plastidreference = "/scratch/fullam/references/oleks/references/v4/concatNonRed98o70_PL_Ref_250121_v1_3.contigs.fasta"

params.mito = true
params.plastid = true

Channel
    .fromPath("${params.sample_folders}/*", type: 'dir')
    .map { file -> tuple(file.name, file) }
    .into { sample_folders; sample_folders1; sample_folders2; sample_folders3 }

sample_folders.subscribe { println it }


def check_max(obj, max) {
    try {
            return obj.compareTo(max) == 1 ? max : obj
    }
    catch( all ) {
        println "[ERROR] Max $key '${max}' is not valid! Using default value: $obj"
        return obj
    }

}


process alignment_mito {

    cpus 4
    memory { check_max(3.GB * task.attempt, 50.GB) }
    time { check_max(15.minutes * task.attempt * task.attempt * 2, 27.hour) }
    module 'bwa/0.7.17'
    module 'samtools/1.7'
    module 'bcftools/1.5.0'
    module 'ngless/1.1.0'
    storeDir "${params.out_dir}/mito/${sample_id}/"

    input:
    set sample_id, sample_folder from sample_folders2

    when:
    params.mito

    output:
    file("${sample_id}.unfiltered.bam")
    file("${sample_id}.filtered.bam")
    file("${sample_id}.unique.bam")
    file("${sample_id}.filtered.vcf.gz")
    file("${sample_id}.filtered.depth.gz")

    script:
    def out_file = "${sample_id}_aligned_to_${sample_id}.depths"
    """
    create_ngless_template_mito.sh
    ngless -t ./ filter_bam.ngl -j 4 ${sample_folder}
    samtools depth filtered.bam > ${sample_id}.filtered.depth
    gzip ${sample_id}.filtered.depth
    bcftools mpileup -f ${params.mitoreference} filtered.bam -O z -o ${sample_id}.filtered.vcf.gz
    mv unfiltered.bam ${sample_id}.unfiltered.bam
    mv filtered.bam ${sample_id}.filtered.bam
    mv unique.bam ${sample_id}.unique.bam
    """
}


process alignment_plastid {

    cpus 4
    memory { check_max(3.GB * task.attempt, 50.GB) }
    time { check_max(15.minutes * task.attempt * task.attempt * 2, 27.hour) }
    module 'bwa/0.7.17'
    module 'samtools/1.7'
    module 'bcftools/1.5.0'
    module 'ngless/1.1.0'
    storeDir "${params.out_dir}/plastid/${sample_id}/"

    input:
    set sample_id, sample_folder from sample_folders3

    when:
    params.plastid

    output:
    file("${sample_id}.unfiltered.bam")
    file("${sample_id}.filtered.bam")
    file("${sample_id}.unique.bam")
    file("${sample_id}.filtered.vcf.gz")
    file("${sample_id}.filtered.depth.gz")

    script:
    def out_file = "${sample_id}_aligned_to_${sample_id}.depths"
    """
    create_ngless_template_plastid.sh
    ngless -t ./ filter_bam.ngl -j 4 ${sample_folder}
    samtools depth filtered.bam > ${sample_id}.filtered.depth
    gzip ${sample_id}.filtered.depth
    bcftools mpileup -f ${params.plastidreference} filtered.bam -O z -o ${sample_id}.filtered.vcf.gz
    mv unfiltered.bam ${sample_id}.unfiltered.bam
    mv filtered.bam ${sample_id}.filtered.bam
    mv unique.bam ${sample_id}.unique.bam
    """
}
