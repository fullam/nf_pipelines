#!/usr/bin/env nextflow
params.workflow_version = "screen_for_pathogens_v0.0.1"
params.reference = "/scratch/fullam/oleks/references/v2/concatenatedReference_All_v1_1.FNA"
params.mitoreference = "/scratch/fullam/references/oleks/references/v3/concatRedundantMT_Ref_250121_v1_3.contigs.fasta"
params.plastidreference = "/scratch/fullam/references/oleks/references/v3/concatRedundantPL_Ref_250121_v1_3.contigs.fasta"

params.all = false
params.mito = true
params.plastid = true

Channel
    .fromPath("${params.sample_folders}*", type: 'dir')
    .map { file -> tuple(file.name, file) }
    .into { sample_folders; sample_folders1; sample_folders2; sample_folders3 }

sample_folders.subscribe { println it }


def check_max(obj, max) {
    try {
            return obj.compareTo(max) == 1 ? max : obj
    }
    catch( all ) {
        println "[ERROR] Max $key '${max}' is not valid! Using default value: $obj"
        return obj
    }

}


process alignment_all {

    cpus 15
    memory { check_max(25.GB * task.attempt, 100.GB) }
    time { check_max(3.hour * task.attempt * task.attempt * 2, 27.hour) }
    module 'bwa/0.7.17'
    module 'samtools/1.7'
    module 'bcftools/1.5.0'
    module 'ngless/1.1.0'
    publishDir "${params.out_dir}/all/", mode: 'copy'

    input:
    set sample_id, sample_folder from sample_folders1

    when:
    params.all

    output:
    set val("${sample_id}"), file("${sample_id}/${sample_id}.bam"), file("${sample_id}/${sample_id}.vcf.gz"), file("${sample_id}/${sample_id}.depth.gz") into all_ch

    script:
    def out_file = "${sample_id}_aligned_to_${sample_id}.depths"
    """
    mkdir ${sample_id}
    cat ${sample_folder}/*.pair.1.fq.gz > ${sample_id}/${sample_id}.pair.1.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.pair.2.fq.gz > ${sample_id}/${sample_id}.pair.2.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.single*.fq.gz > ${sample_id}/${sample_id}.singles.fq.gz 2>/dev/null &
    wait
    find ${sample_id}/${sample_id}.*.fq.gz -empty -type f -delete


    if [ -f "${sample_id}/${sample_id}.pair.2.fq.gz" ]; then
        if [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.reference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > tmp_paired.bam &
            bwa mem -v 2 -t 8 ${params.reference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > tmp_singles.bam &
            wait
            samtools merge ${sample_id}.bam tmp_paired.bam tmp_singles.bam
        else
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.reference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > ${sample_id}.bam &
        fi
    elif [ -f "${sample_id}/${sample_id}.pair.1.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.reference} ${sample_id}/${sample_id}.pair.1.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.reference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.reference} ${sample_id}/${sample_id}.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam
    fi

    wait
    find ${sample_id}.bam -empty -type f -delete
    create_ngless_template.sh
    ngless -t ./ filter_bam.ngl -j 4 ${sample_id}.bam
    samtools sort -m 16G -@4 filtered.bam > ${sample_id}/${sample_id}.bam
    samtools depth ${sample_id}/${sample_id}.bam > ${sample_id}/${sample_id}.depth
    gzip ${sample_id}/${sample_id}.depth
    bcftools mpileup -f ${params.reference} ${sample_id}/${sample_id}.bam -O z -o ${sample_id}/${sample_id}.vcf.gz
    """
}


process alignment_mito {

    cpus 4
    memory { check_max(3.GB * task.attempt, 50.GB) }
    time { check_max(15.minutes * task.attempt * task.attempt * 2, 27.hour) }
    module 'bwa/0.7.17'
    module 'samtools/1.7'
    module 'bcftools/1.5.0'
    module 'ngless/1.1.0'
    publishDir "${params.out_dir}/mito/", mode: 'copy'

    input:
    set sample_id, sample_folder from sample_folders2

    when:
    params.mito

    output:
    set val("${sample_id}"), file("${sample_id}/${sample_id}.bam"), file("${sample_id}/${sample_id}.vcf.gz"), file("${sample_id}/${sample_id}.depth.gz") into mito_ch

    script:
    def out_file = "${sample_id}_aligned_to_${sample_id}.depths"
    """
    mkdir ${sample_id}
    cat ${sample_folder}/*.pair.1.fq.gz > ${sample_id}/${sample_id}.pair.1.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.pair.2.fq.gz > ${sample_id}/${sample_id}.pair.2.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.single*.fq.gz > ${sample_id}/${sample_id}.singles.fq.gz 2>/dev/null &
    wait
    find ${sample_id}/${sample_id}.*.fq.gz -empty -type f -delete


    if [ -f "${sample_id}/${sample_id}.pair.2.fq.gz" ]; then
        if [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.mitoreference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > tmp_paired.bam &
            bwa mem -v 2 -t 8 ${params.mitoreference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > tmp_singles.bam &
            wait
            samtools merge ${sample_id}.bam tmp_paired.bam tmp_singles.bam
        else
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.mitoreference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > ${sample_id}.bam &
        fi
    elif [ -f "${sample_id}/${sample_id}.pair.1.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.mitoreference} ${sample_id}/${sample_id}.pair.1.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.mitoreference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.mitoreference} ${sample_id}/${sample_id}.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam
    fi

    wait
    find ${sample_id}.bam -empty -type f -delete
    create_ngless_template_mito.sh
    ngless -t ./ filter_bam.ngl -j 4 ${sample_id}.bam
    samtools sort -m 16G -@4 filtered.bam > ${sample_id}/${sample_id}.bam
    samtools depth ${sample_id}/${sample_id}.bam > ${sample_id}/${sample_id}.depth
    gzip ${sample_id}/${sample_id}.depth
    bcftools mpileup -f ${params.mitoreference} ${sample_id}/${sample_id}.bam -O z -o ${sample_id}/${sample_id}.vcf.gz
    """
}


process alignment_plastid {

    cpus 4
    memory { check_max(3.GB * task.attempt, 50.GB) }
    time { check_max(15.minutes * task.attempt * task.attempt * 2, 27.hour) }
    module 'bwa/0.7.17'
    module 'samtools/1.7'
    module 'bcftools/1.5.0'
    module 'ngless/1.1.0'
    publishDir "${params.out_dir}/plastid/", mode: 'copy'

    input:
    set sample_id, sample_folder from sample_folders3

    when:
    params.plastid

    output:
    set val("${sample_id}"), file("${sample_id}/${sample_id}.bam"), file("${sample_id}/${sample_id}.vcf.gz"), file("${sample_id}/${sample_id}.depth.gz") into plastid_ch

    script:
    def out_file = "${sample_id}_aligned_to_${sample_id}.depths"
    """
    mkdir ${sample_id}
    cat ${sample_folder}/*.pair.1.fq.gz > ${sample_id}/${sample_id}.pair.1.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.pair.2.fq.gz > ${sample_id}/${sample_id}.pair.2.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.single*.fq.gz > ${sample_id}/${sample_id}.singles.fq.gz 2>/dev/null &
    wait
    find ${sample_id}/${sample_id}.*.fq.gz -empty -type f -delete


    if [ -f "${sample_id}/${sample_id}.pair.2.fq.gz" ]; then
        if [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.plastidreference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > tmp_paired.bam &
            bwa mem -v 2 -t 8 ${params.plastidreference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > tmp_singles.bam &
            wait
            samtools merge ${sample_id}.bam tmp_paired.bam tmp_singles.bam
        else
            read_pair="${sample_id}/${sample_id}.pair.1.fq.gz ${sample_id}/${sample_id}.pair.2.fq.gz"
            bwa mem -v 2 -t 8 ${params.plastidreference} \${read_pair} | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4  > ${sample_id}.bam &
        fi
    elif [ -f "${sample_id}/${sample_id}.pair.1.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.plastidreference} ${sample_id}/${sample_id}.pair.1.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.singles.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.plastidreference} ${sample_id}/${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam

    elif  [ -f "${sample_id}/${sample_id}.fq.gz" ]; then
        bwa mem -v 2 -t 8 ${params.plastidreference} ${sample_id}/${sample_id}.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 16G -@4 > ${sample_id}.bam
    fi

    wait
    find ${sample_id}.bam -empty -type f -delete
    create_ngless_template_plastid.sh
    ngless -t ./ filter_bam.ngl -j 4 ${sample_id}.bam
    samtools sort -m 16G -@4 filtered.bam > ${sample_id}/${sample_id}.bam
    samtools depth ${sample_id}/${sample_id}.bam > ${sample_id}/${sample_id}.depth
    gzip ${sample_id}/${sample_id}.depth
    bcftools mpileup -f ${params.plastidreference} ${sample_id}/${sample_id}.bam -O z -o ${sample_id}/${sample_id}.vcf.gz
    """
}
