#!/usr/bin/env bash

cat  <<EOF > filter_bam.ngl
ngless "1.1"
import "parallel" version "1.0"
import "mocat" version "0.0"
import "samtools" version "0.0"

insamfile = samfile(ARGV[1])
insamfile = select(insamfile) using |mr|:
    mr = mr.filter(min_match_size=70, min_identity_pc=98, action={drop})
    if not mr.flag({mapped}):
        discard

write(insamfile, ofile='filtered.bam' )
EOF
