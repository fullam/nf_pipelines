#!/usr/bin/env nextflow

params.workflow_version = "rnaseq_0.1"
params.reference = "/g/scb2/bork/fullam/referencees/NT5078-Escherichia_coli_ED1a_index/NT5078-Escherichia_coli_ED1a"
params.gene_annotation = "/g/scb/bork/ralves/projects/mock_experiment/genomes/selected_genomes_2nd_iteration/annotations/NT5078-Escherichia_coli_ED1a.prokka/NT5078-Escherichia_coli_ED1a-prokka.gff"

Channel
    .fromPath(params.sample_folders, type: 'dir')
    .map { file -> tuple(file.name, file) }
    .into { input_samples1; input_samples2 ; input_samples3}


input_samples2.subscribe { println it }


def check_max(obj, max) {
    try {
            return obj.compareTo(max) == 1 ? max : obj
    }
    catch( all ) {
        println "[ERROR] Max $key '${max}' is not valid! Using default value: $obj"
        return obj
    }

}

process merge_fastqs {

    cpus 1
    memory {2.GB * task.attempt }
    time { 15.min * task.attempt }

    input:
    set sample_id, sample_folder from input_samples3

    output:
    set val("${sample_id}"), file("${sample_id}/*fq.gz") into merged_fastqs

    script:
    """
    mkdir ${sample_id}
    cat ${sample_folder}/*.pair.1.fq.gz > ${sample_id}/${sample_id}.untrimmed.pair.1.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.pair.2.fq.gz > ${sample_id}/${sample_id}.untrimmed.pair.2.fq.gz 2>/dev/null &
    cat ${sample_folder}/*.single.fq.gz > ${sample_id}/${sample_id}.untrimmed.singles.fq.gz 2>/dev/null &
    wait
    find ${sample_id}/${sample_id}.*.fq.gz -empty -type f -delete
    """
}

process cut_adapters {

    cpus 4
    memory { 4.GB * task.attempt }
    time { 1.hour * task.attempt * task.attempt }
    module 'cutadapt/2.3'
    publishDir "${params.out_dir}${sample_id}", pattern: '**{log}', mode: 'copy'

    input:
    set val(sample_id), file('*') from merged_fastqs

    output:
    set val("${sample_id}"), file("${sample_id}/*fq.gz") into (trimmed_fastqs, trimmed_fastqs2)
    file("logs/${sample_id}.cutadapt.log")  into cutadapt_log_files_ch

    script:
    """
    mkdir ${sample_id}
    if [ -f "${sample_id}.untrimmed.pair.2.fq.gz" ]; then
        cutadapt -a AGATCGGAAGAG -A AGATCGGAAGAG -j 4 -q 20 --minimum-length 15 -o ${sample_id}/${sample_id}.pair.1.fq.gz -p ${sample_id}/${sample_id}.pair.2.fq.gz ${sample_id}.untrimmed.pair.1.fq.gz ${sample_id}.untrimmed.pair.2.fq.gz
    fi
    if [ -f "${sample_id}.untrimmed.singles.fq.gz" ]; then
        cutadapt -a AGATCGGAAGAG -j 4 -q 20 --minimum-length 15 -o ${sample_id}/${sample_id}.singles.fq.gz ${sample_id}.untrimmed.singles.fq.gz
    fi
    mkdir logs
    cp .command.log logs/${sample_id}.cutadapt.log
    """
}

process fastqc {

    cpus 1
    memory { 2.GB * task.attempt }
    time { 30.min * task.attempt }
    publishDir "${params.out_dir}${sample_id}/fastqc/", mode: 'copy'
    module 'fastqc/0.11.5'

    input:
    set val(sample_id), file('*') from trimmed_fastqs

    output:
    file "*_fastqc.html" into fastqc_ch
    file "*_fastqc.zip" into fastqc_zip_ch

    script:
    """
    fastqc *.fq.gz -o ./
    """

}


process alignment {

    cpus 30
    memory { check_max(20.GB * task.attempt, 100.GB) }
    time { check_max(1.hour * task.attempt * task.attempt * 2, 27.hour) }
    module 'bowtie2/2.3.4.3'
    module 'samtools/1.7'
    publishDir "${params.out_dir}${sample_id}", pattern: '**{bam,log}', mode: 'copy'

    input:
    set val(sample_id), file('*') from trimmed_fastqs2

    output:
    set val("${sample_id}"), env(READS), file("${sample_id}.bam")  into bam_files_ch
    file("logs/${sample_id}.bowtie.log")  into log_files_ch

    script:
    """
    if [ -f "${sample_id}.pair.2.fq.gz" ]; then
        READS=paired_end
        if [ -f "${sample_id}.single.fq.gz" ]; then
          bowtie2 -X 1000 -p 24 -x ${params.reference} -1 ${sample_id}.pair.1.fq.gz -2 ${sample_id}.pair.2.fq.gz -U ${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 8G -@4  > ${sample_id}.bam
        else
          bowtie2 -X 1000 -p 24 -x ${params.reference} -1 ${sample_id}.pair.1.fq.gz -2 ${sample_id}.pair.2.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 8G -@4  > ${sample_id}.bam
        fi
    else
        READS=single_end
        bowtie2 -X 1000 -p 24 -x ${params.reference} -U ${sample_id}.singles.fq.gz | samtools view -hb -u -F 4 -  | samtools sort -m 8G -@4  > ${sample_id}.bam
    fi
    mkdir logs
    cp .command.log logs/${sample_id}.bowtie.log
    """
}


process count_features {

    cpus 1
    memory { check_max(2.GB * task.attempt, 20.GB) }
    time { 15.min * task.attempt * task.attempt}
    module 'rsubread/2.0.0'
    publishDir "${params.out_dir}${sample_id}", mode: 'copy'

    input:
    set val(sample_id), env(READS), val(bam) from bam_files_ch

    output:
    file("${sample_id}_count.tsv") into counts_ch
    file("logs/${sample_id}.count.log") into logs_ch

    script:
    """
    mkdir logs
    count_features.R ${bam} \$READS ./ ${params.gene_annotation}
    mv ${sample_id}.tsv ${sample_id}_count.tsv
    cp .command.log logs/${sample_id}.count.log
    """
}
