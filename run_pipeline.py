#!/usr/bin/env python3
import os
import sys
import socket
import argparse
import subprocess


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--pipeline_name',
                        help='Name of pipeline to run.',
                        metavar='',
                        default=False)
    parser.add_argument('-i', '--info',
                        help='Print information about a pipeline.',
                        metavar='',
                        default=False)
    parser.add_argument('-l', '--list',
                        help='List all pipelines',
                        action='store_true',
                        default=False)
    parser.add_argument('-o', '--out_dir',
                        help='Output Directory.',
                        metavar='',
                        default=False)
    parser.add_argument('-z', '--dryrun',
                        help='Print slurm commands instead of submitting jobs',
                        action='store_true',
                        default=False)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    return parser.parse_known_args()


def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def sbatch(cmd, job_name, log_file, c, time, mem, partition, dependency=None):
    if dependency:
        dependency = '--dependency=afterany:{}'.format(dependency)
    else:
        dependency = ''
    sbatch_cmd = (f'sbatch '
                  f'--job-name={job_name} '
                  f'--output={log_file} '
                  f'--mem={mem} '
                  f'-c {c}  '
                  f'-t {time}  '
                  f'-p {partition} '
                  f'{dependency} '
                  f'--wrap="{cmd}"')
    if args.dryrun:
        print('\n', sbatch_cmd, '\n')
        return ''
    else:
        return sbatch_cmd


def check_args(expected_args, given_args):
    for argument in expected_args:
        if argument in given_args:
            continue
        else:
            sys.exit(f'Required arg: {argument}: {expected_args[argument]}')


def pprint(data):
    for x in data:
        print(f'\t{x}:  {data[x]}')


def print_pipeline_info(info):
    print(f'----------------  {info["name"]}  ----------------')
    print(f'\nType: {"Pipeline" if info["pipeline"] else "Script"}\n')
    print(f'\n{info["info"]}\n\nRequired Args:\n')
    pprint(info['req_args'])
    print(f'\nOptional Args:\n')
    pprint(info['opt_args'])
    print(f'\nPath : {info["path"]}\n')
    print(f'Run Example: : {info["running_example"]}\n')


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    args, unknownargs = parse_args(args)

    script_dir = os.path.dirname(os.path.realpath(__file__))

    pipelines = {'rnaseq': {'name': 'RnaSeq Pipeline',
                            'pipeline': True,
                            'info': 'RnaSeq analysis pipeline.',
                            'req_args': {'--sample_folders': 'Directory containing sample folders (mocat format).'},
                            'opt_args': {'--reference': 'Bowtie reference to map to. Defaults to Ecoli',
                                         '--gene_annotation': 'Refseq-generated GFF file that contains all annotated genes of reference genome'},
                            'path': f'{script_dir}/rnaseq/rnaseq.nf',
                            'running_example': 'pipeline -p rnaseq --sample_folders "./[STUDY]/*"'},
                 'screen_for_pathogens': {'name': 'Pathogen Screen',
                            'pipeline': True,
                            'info': 'Screens samples for pathogens',
                            'req_args': {'--sample_folders': 'Directory containing sample folders (mocat format).'},
                            'opt_args': {'--reference': 'Reference to map to.'},
                            'path': f'{script_dir}/screen_for_pathogens/main.nf',
                            'running_example': 'pipeline -p screen_for_pathogens --sample_folders [SAMPLE_FOLDER]'},
                 'ena_sample_info': {'name': 'Get ENA Metadata',
                                 'pipeline': False,
                                 'info': 'Gets runreports from ENA.',
                                 'req_args': {'-o': 'output file.'},
                                 'opt_args': {'-p': 'Comma seperated list of ENA study ids',
                                              '-s': 'Comma seperated list of ENA sample ids'},
                                 'path': f'{script_dir}/get_sample_info',
                                 'running_example': 'get_sample_info -p PRJNA464578 -o out'},
                 'motus_search': {'name': 'Get motus data',
                                 'pipeline': False,
                                 'info': 'Gets motus data and microontology.',
                                 'req_args': {'-m': 'mOTU to search for e.g. ref_mOTU_v25_11945.',
                                              '-o': 'output file.'},
                                 'opt_args': {'-g': 'markergenes. Default: 3',
                                              '-c': 'Add column with total number of samples (with motus) that have this metadata annotation'},
                                 'path': f'{script_dir}/motus_search',
                                 'running_example': 'motus_search -m ref_mOTU_v25_00721 -o test_out.tsv'}}

    if args.list:
        print('Available functions: ', ', '.join(pipelines.keys()))
        sys.exit()

    if args.info:
        if args.info == 'all':
            for pipeline in pipelines:
                print_pipeline_info(pipelines[pipeline])
            sys.exit()
        info = pipelines.get(args.info, None)
        if info:
            print_pipeline_info(info)
            sys.exit()
        else:
            sys.exit(f'[ERROR] {args.info} unknown.')

    info = pipelines.get(args.pipeline_name,
                         f'{args.pipeline_name} not found.')
    if not info["pipeline"]:
        print_pipeline_info(info)
        sys.exit()
    check_args(info['req_args'], unknownargs)


    if not args.out_dir:
        out_dir = os.path.join(os.getcwd(), 'output/')
    else:
        out_dir = os.path.join(args.out_dir, '')

    create_dir(out_dir)

    if socket.gethostname() == 'login.cluster.embl.de':
        partition = 'htc'
        config = f" -C {script_dir}/{args.pipeline_name}/embl_cluster.config "
    else:
        partition = 'CLUSTER'
        config = f" -C {script_dir}/{args.pipeline_name}/bork_cluster.config "

    command = f"""nextflow {config} run {pipelines[args.pipeline_name]['path']} -with-report {out_dir}report.html -with-timeline {out_dir}timeline.html -with-trace {out_dir}trace.txt --out_dir {out_dir} {' '.join(unknownargs)}"""

    command = command.replace('*', '\*')

    logfile = f'slurm_%j.out'
    jobname = f'nf_pipeline_master_{args.pipeline_name}'
    jobid = subprocess.check_output(sbatch(command,
                                           jobname,
                                           logfile,
                                           2,
                                           '14-00:00:00',
                                           '10G',
                                           partition),
                                    shell=True,
                                    universal_newlines=True).split(' ')[-1]

    if not args.dryrun:
        print(f'JobID: {jobid}')
